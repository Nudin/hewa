# hewa -- Allow your readers to report typos and other corrections.
# Copyright (C) 2018 Michael Schönitzer
# SPDX-License-Identifier: GPL-3.0-or-later
import difflib
import re
from colorama import Fore, Style


class ChangedParagraphs:
    """
    Iterator over all paragraphs that have changed

    FIXME: This currently is implemented as a ugly wrapper on difflibs
    compare().  I should probably reimplement this from scratch.
    """
    def __init__(self, from_text, to_text):
        self.d = difflib.Differ()
        self.from_paragraph = ""
        self.to_paragraph = ""
        self.from_paragraphs = self.__intersperse__(re.split(r'\n\n|</?p>', from_text), '-')
        self.to_paragraphs = self.__intersperse__(re.split(r'\n\n|</?p>', to_text), '-')
        self.from_paragraphs = list(map(str.strip, self.from_paragraphs))
        self.to_paragraphs = list(map(str.strip, self.to_paragraphs))

    def __iter__(self):
        for line in self.d.compare(self.from_paragraphs, self.to_paragraphs):
            if line[0] == ' ':
                if self.from_paragraph == self.to_paragraph:
                    continue
                yield (self.from_paragraph, self.to_paragraph)
                self.from_paragraph = ""
                self.to_paragraph = ""
            elif line[0] == '-':
                self.from_paragraph += line[2:]
            elif line[0] == '+':
                self.to_paragraph += line[2:]

    def __intersperse__(self, lst, item):
        result = [item] * (len(lst) * 2 - 1)
        result[0::2] = lst
        return result


def diffratio(old, new):
    """
    Calculate the similarity of two texts
    """
    diff = difflib.SequenceMatcher(None, old, new)
    return diff.ratio()


def terminaldiff(old, new):
    """
    Generate a colorful diff for output on command line.
    """
    old = old.splitlines()
    new = new.splitlines()
    d = difflib.Differ()
    diff = d.compare(old, new)
    lines = []
    for line in diff:
        if line[0] == '+':
            line = Fore.GREEN + line + Style.RESET_ALL
        elif line[0] == '-':
            line = Fore.RED + line + Style.RESET_ALL
        elif line[0] == '?':
            line = Fore.YELLOW + line + Style.RESET_ALL
        lines.append(line)
    return '\n'.join(lines)
