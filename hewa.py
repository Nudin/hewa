#!/usr/bin/env python3
# hewa -- Allow your readers to report typos and other corrections.
# Copyright (C) 2018 Michael Schönitzer
# SPDX-License-Identifier: GPL-3.0-or-later
import functools
import hashlib
import os.path
import random
import sqlite3
import unicodedata
import urllib.parse
from itertools import chain

from flask import Flask, g, redirect, request
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from lxml import etree

import conf
from diff import ChangedParagraphs, diffratio

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path="/")
app.config["MAX_CONTENT_LENGTH"] = 100 * 1024
limiter = Limiter(app, key_func=get_remote_address, default_limits=[])


@functools.lru_cache()
def loadhtml(filename):
    with open(filename, "r") as f:
        root = etree.HTML(f.read())
        node = root.xpath("//div[@class='editable']")[0]
        # TODO: Make this readable
        return "".join(
            chunk
            for chunk in chain(
                (node.text,),
                chain(
                    *(
                        (
                            etree.tostring(child, with_tail=False, encoding="unicode"),
                            child.tail,
                        )
                        for child in node.getchildren()
                    )
                ),
                (node.tail,),
            )
            if chunk
        )


def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(conf.DATABASE)
    return db


@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, "_database", None)
    if db is not None:
        db.commit()
        db.close()


def init_db():
    with app.app_context():
        db = get_db()
        with app.open_resource("schema.sql", mode="r") as f:
            db.cursor().executescript(f.read())
        db.commit()


def hash_string(string):
    return hashlib.md5(string.encode("utf-8")).hexdigest()


@app.route("/sendchange", methods=["POST"])
@limiter.limit(["50 per day", "20 per hour"])
def sendchange():
    answer = request.get_json()
    filename = urllib.parse.unquote(answer["filename"])
    if filename.startswith(conf.ROOT):
        prefix = len(conf.ROOT)
        filename = filename[prefix:]
    if filename.startswith("/"):
        filename = filename[1:]
    try:
        from_text = loadhtml(os.path.join(conf.HTMLDIR, filename)).strip()
    except Exception:
        return "Error: couldn't load file %s" % filename, 406
    to_text = answer["to"].strip()
    if any(unicodedata.category(ch)[0] == "C" and ch not in "\t\n\r" for ch in to_text):
        return "Error: Text contains fishy control sequences", 406

    ratio = diffratio(from_text, to_text)
    print(ratio)
    if ratio == 1:
        return "No change has been made", 201
    if ratio < conf.min_distance:
        return "Sorry this tool is only meant for correction for small mistakes.", 406

    identity = hash_string(salt + request.remote_addr)

    c = get_db().cursor()
    for (from_paragraph, to_paragraph) in ChangedParagraphs(from_text, to_text):
        from_text_par_id = hash_string(from_paragraph)
        to_text_par_id = hash_string(to_paragraph)
        try:
            c.execute(
                "INSERT OR REPLACE INTO edits VALUES (?,?,?,?, DATETIME('now'))",
                (filename, from_text_par_id, to_text_par_id, identity),
            )
            c.execute(
                "INSERT OR REPLACE INTO revisions VALUES (?,?)",
                (to_text_par_id, to_paragraph),
            )
            c.execute(
                "INSERT OR REPLACE INTO revisions VALUES (?,?)",
                (from_text_par_id, from_paragraph),
            )
        except sqlite3.IntegrityError:
            print("IntegrityError")

    return "Thanks!", 202


@app.route("/")
def root():
    return redirect("./hewa.html", code=302)


@app.route("/<name>")
def render_static(name):
    return app.send_static_file(name)


salt = str(random.getrandbits(256))

init_db()
