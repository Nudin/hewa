#!/usr/bin/env python3
# hewa -- Allow your readers to report typos and other corrections.
# Copyright (C) 2018 Michael Schönitzer
# SPDX-License-Identifier: GPL-3.0-or-later
import os.path
import sqlite3

from diff import terminaldiff
import conf

conn = sqlite3.connect(conf.DATABASE)
c = conn.cursor()
filecache = {}


def ask(question, options):
    while True:
        optionsstr = options[0].upper() + '/' + '/'.join(options[1:])
        answer = str.lower(input("%s [%s] " % (question, optionsstr)))
        if answer == "":
            return options[0]
        elif len([o for o in options if o.lower().find(answer) == 0]) == 1:
            return [o for o in options if o.lower().find(answer) == 0][0]
        else:
            continue


def writechage(filename, oldtext, text):
    global filecache
    if filename not in filecache:
        with open(filename, "r") as f:
            print("cachemiss")
            filecache[filename] = f.read()
    filecache[filename] = mergechange(filecache[filename], oldtext, text)
    with open(filename, "w") as f:
        f.write(filecache[filename])


def mergechange(filecontent, oldtext, text):
        pos = filecontent.find(oldtext)
        if pos == -1:
            raise RuntimeError("Error: text not found.")
        elif filecache[filename].find(oldtext, pos+1) != -1:
            raise NotImplementedError("Error: Text not unique. Automerge not possible")
        else:
            return filecontent.replace(oldtext, text)


def getrevtext(conn, rev):
    cur = conn.cursor()
    cur.execute('select text from revisions where id = ?', (rev,))
    return cur.fetchone()[0]


dupplicatereports = c.execute('''select file, from_id, to_id, count(user_id) as count
                    from edits group by file, from_id, to_id
                    having count>1 order by count desc''')

for (filename, from_id, to_id, count) in dupplicatereports:
    fullfilename = os.path.join("static", filename)
    oldtext = getrevtext(conn, from_id)
    text = getrevtext(conn, to_id)
    print("Submitted by %i users:" % count)
    print(terminaldiff(oldtext, text))
    answer = ask("Apply?", ['Skip', 'Apply', 'Drop'])
    print(answer)
    if answer == 'Skip':
        continue
    elif answer == 'Apply':
        mergechange(fullfilename, oldtext, text)
        c.execute("DELETE FROM edits WHERE file=? and from_id=? and to_id=?",
                  (filename, from_id, to_id))
    elif answer == 'Drop':
        c.execute("DELETE FROM edits WHERE file=? and from_id=? and to_id=?",
                  (filename, from_id, to_id))
    conn.commit()

singlereports = c.execute('''select file, from_id, to_id, user_id, count(user_id) as count
                        from edits group by file, from_id, to_id having count=1''')
for (filename, from_id, to_id, user_id, date) in singlereports:
    fullfilename = os.path.join("static", filename)
    oldtext = getrevtext(conn, from_id)
    text = getrevtext(conn, to_id)
    print("Submitted by user %s:" % user_id)
    print(terminaldiff(oldtext, text))
    answer = ask("Apply?", ['Skip', 'Apply', 'Drop', 'Ignore User'])
    if answer == 'Skip':
        continue
    elif answer == 'Apply':
        mergechange(fullfilename, oldtext, text)
        c.execute("DELETE FROM edits WHERE file=? and from_id=? and to_id=?",
                  (filename, from_id, to_id))
    elif answer == 'Drop':
        c.execute("DELETE FROM edits WHERE file=? and from_id=? and to_id=?",
                  (filename, from_id, to_id))
    elif answer == 'Ignore User':
        c.execute("DELETE FROM edits WHERE user_id=?", (user_id,))
    conn.commit()

conn.close()
