// hewa – Allow your readers to report typos and other corrections.
// Copyright (C) 2018 Michael Schönitzer
// SPDX-License-Identifier: GPL-3.0-or-later

document.addEventListener("DOMContentLoaded", function(event) { 
	var byId = function( id ) { return document.getElementById( id ); };
	var byClass = function( cl ) { return document.getElementsByClassName( cl ); };
	var setDisplay = function(id, mode) { byId(id).style.display = mode; }


	function show_message(msg, status) {
		console.log(msg);
		console.log(status)
		byId('message').textContent = msg;
		if (status < 400) {
			byId('message').style.background = "lightgreen";
		}
		else {
			byId('message').style.background = "#ff8080";
		}
		setDisplay('message', "block");
		window.setTimeout(function() { setDisplay('message', 'none'); },
						  2000);
	};


	function edit() {
		byClass('editable')[0].contentEditable = 'true';
		setDisplay('save', "block");
		setDisplay('edit', "none");
	};


	function save() {
		byClass('editable')[0].contentEditable = 'false';
		setDisplay('save', "none");
		setDisplay('edit', "");
		newContent = byClass('editable')[0].innerHTML;

		var request = new XMLHttpRequest();
		request.onload = function() {
			show_message(this.response, this.status);
		};
		request.onerror = function() {
			show_message("Error conecting to server", 400);
		};
		request.open('POST', './sendchange', true);
		request.setRequestHeader('Content-Type', 'application/json');
		request.send(JSON.stringify({
			filename: window.location.pathname,
			to: newContent
		}));
	};
	
	if(byClass('editable').length < 1) {
		return;
	}
	if(!byId('edit')) {
		var div = document.createElement("div");
		div.id = "edit";
		div.appendChild(document.createTextNode("✎ Edit"));
		document.body.appendChild(div);
	}
	if(!byId('save')) {
		var div = document.createElement("div");
		div.id = "save";
		div.appendChild(document.createTextNode("✓ Save"));
		document.body.appendChild(div);
	}
	if(!byId('message')) {
		var div = document.createElement("div");
		div.id = "message";
		document.body.appendChild(div);
	}
	byId('edit').onclick = edit;
	byId('save').onclick = save;
});
