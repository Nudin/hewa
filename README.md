# Hewa
Allow the readers of your blog/homepage to report typos and other corrections.

## How
Pure Javascript on client side Python (flask) backend.

Frontend sends text to backend. Backend saves diff in sqlite-db.

## Install (Nginx & uWSGI)
sudo apt-get install python3-flask python3-lxml
sudo apt-get install uwsgi uwsgi-plugin-python3
pip3 install flask-limiter
set DATABASE in conf.py
sudo uwsgi -s /tmp/hewa.sock --manage-script-name --mount /hewa=hewa:app --uid www-data --gid www-data --plugin python3

http://flask.pocoo.org/docs/1.0/deploying/uwsgi/#configuring-nginx

## TODOs

- avoid having to edit the code path of sendsection in js-file
- Beautify the layout
- Clean up code
- Write diff-algorithm by scratch
- Admin Web-interface
