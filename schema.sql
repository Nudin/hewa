CREATE TABLE IF NOT EXISTS edits
             (file text, from_id text, to_id text, user_id text, edit_date text,
             CONSTRAINT unique_row UNIQUE (file, from_id, to_id, user_id));

CREATE TABLE IF NOT EXISTS revisions
             (id text, text text,
             PRIMARY KEY(id));
